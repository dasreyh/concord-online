using Godot;
using RestSharp;
using System;
using System.IO;
using System.Net;

public class MainMenu : Control
{
    private string client_id = "470944608281690142";
    private string client_sceret = "4pakgvZ2OE7C1YafFa7kOF96p03nOOkZ";
    private string redirect_url = "http://finiteskies.com";

    public override void _Ready()
    {
        // Called every time the node is added to the scene.
        // Initialization here
        GD.Printt("Ready! Calling API...");
        var uri = "https://discordapp.com/api/oauth2/authorize?response_type=code&client_id=" + client_id + "&scope=identify%20guilds.join&state=15773059ghq9183habn&redirect_uri=" + redirect_url + "";
        OS.ShellOpen(uri);
        
        CallAPI();
    }

    public void CallAPI()
    {
        var client = new RestClient("https://discordapp.com/api/oauth2/token");
        var request = new RestRequest(Method.POST);
        request.AddHeader("cache-control", "no-cache");
        request.AddHeader("content-type", "application/x-www-form-urlencoded");
        string parameters = "client_id=" + client_id + "&client_secret=" + client_sceret + "&grant_type=authorization_code&code=" + code + "&redirect_uri=" + redirect_url + "";
        request.AddParameter("application/x-www-form-urlencoded", parameters, ParameterType.RequestBody);
        IRestResponse response = client.Execute(request);
        GD.Print("CallAPI Response:" + response.Content);
    }

    public string GetDiscordClientInfo(string access_token)
    {
        HttpWebRequest webRequest1 = (HttpWebRequest)WebRequest.Create("https://discordapp.com/api/users/@me");
        webRequest1.Method = "Get";
        webRequest1.ContentLength = 0;
        webRequest1.Headers.Add("Authorization", "Bearer " + access_token);
        webRequest1.ContentType = "application/x-www-form-urlencoded";

        string apiResponse1 = "";
        using (HttpWebResponse response1 = webRequest1.GetResponse() as HttpWebResponse)
        {
            StreamReader reader1 = new StreamReader(response1.GetResponseStream());
            apiResponse1 = reader1.ReadToEnd();
        }

        string info1 = apiResponse1.Substring(1, apiResponse1.Length - 2).Replace(",", "<br/>");

        return info1;
    }

}
